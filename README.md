# RSS Announcer

Pulls in a list of RSS feeds and announces new posts on slack, rocket chat 
or discord (and potentially other chat platforms on request)

## Instructions for use

Clone the repo 

Get the modules

```
npm install
```
create a credentials.json with the correct urls and so on (see example)

test the credentials
```
node testcreds.js 
```
if it worked add some feeds to sources.json (again see example)

run the main script

```
node index.js
```

It will annouce all recent posts (last 24 hours) in the feeds and then 
update sources.json to the newest post. The it sets intervals to check
each feed. By default this is 900secs (15 mins) but you can override this
by putting a freq value in the json for the feed. The freq is in minutes

If it seems happy, CTRL-C and run it again as a background task (node index.js & )

Also add the following to /etc/rc.local so that it runs on start update
```
cd /path/to/rss-annoucer/; node index.js &
```
You may want to use sudo -u to run as a different user than root in rc.local

