/* RSS Announcer (c) 2021 Hellokitty@goatse.cx
 * 
 * This script tests all the credentials by sending a test message 
 */
'use strict';

const fetch = require('node-fetch'); // for fetching the feed
const Discord = require('discord.js'); // announcing to discord
const { IncomingWebhook } = require('@slack/webhook'); // announcing to slack

var creds = require('./credentials.json'); 

function rockethook(msg, url) {
	var body = {"text":msg}

	return fetch(url, {
			method: 'post',
			body:    JSON.stringify(body),
			headers: { 'Content-Type': 'application/json' },
		});
}

// create hooks once 
for (var i=0; i<creds.length; i++) {
	switch(creds[i].service) {
		case "slack":
			creds[i]["hook"]=new IncomingWebhook(creds[i].url);
		    break;
//		case "rocket":
//		    creds[i]["hook"]=rockethook;
//		    break;
		case "discord":
			// match https://discord.com/api/webhooks/12345678910/T0kEn0fw3Bh00K
			var m=creds[i].url.match(/webhooks\/([^\/]+)\/(.+)/);
//			console.log(m[1], m[2]);
//			process.exit();
		    creds[i]["hook"] = new Discord.WebhookClient(m[1], m[2]);
		    break;
	} 
}

function announcement(text) {
	for (var i=0; i<creds.length; i++) {
		switch(creds[i].service) {
			case "slack":
				creds[i].hook.send({text: text})
				.then(console.log("sent slack "+ text))
			    .catch(console.error);
				break;
			case "rocket":
				rockethook(text,creds[i].url)
				.then(console.log("sent rocket "+ text))
			    .catch(console.error);
				break;
			case "discord":
				creds[i].hook.send(text , {username: creds[i].postID})
				.then(console.log("sent discord "+text))
			    .catch(console.error);
				break;
		} 
	}
}


if (process.argv.length>2) {
	announcement (process.argv.slice(2).join(' '))
} else {
	announcement ("Test announcement to all the places with a URL - https://micronetia.devtru.st/");
}

// ensure we exit after 5 seconds
setTimeout(process.exit, 5000)
