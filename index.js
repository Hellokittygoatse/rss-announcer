/* RSS Announcer (c) 2021 Hellokitty@goatse.cx
 * 
 * This script will scan a list of blog feeds and make announcements to 
 * chat servers that it has the credentials for. Currently supports slack
 * discord and rocket.chat
 */
'use strict';

const FeedParser = require('feedparser'); //
const fetch = require('node-fetch'); // for fetching the feed
const fs = require('fs'); // to update sources.json
const Discord = require('discord.js'); // announcing to discord
const { IncomingWebhook } = require('@slack/webhook'); // announcing to slack

// sources.json is an array of rss sources with (optionally) a last seen date 
// see sources.json.example as an example (note REMOVE the example.com one)
var sources = require('./sources.json');
// credentials.json is an array of slack/discord webhook credentials
// see crfedentials.json.example as an example. You can have multiple
// slack and discord bots. Currently all feeds are announced to all bots
var creds = require('./credentials.json'); 

function rockethook(msg, url) {
	var body = {"text":msg}

	fetch(url, {
			method: 'post',
			body:    JSON.stringify(body),
			headers: { 'Content-Type': 'application/json' },
		})
		.catch(err => console.error(err));
}

// create hooks once 
for (var i=0; i<creds.length; i++) {
	switch(creds[i].service) {
		case "slack":
			creds[i]["hook"]=new IncomingWebhook(creds[i].url);
		    break;
//		case "rocket":
//		    creds[i]["hook"]=rockethook;
//		    break;
		case "discord":
			// match https://discord.com/api/webhooks/12345678910/T0kEn0fw3Bh00K
			var m=creds[i].url.match(/webhooks\/([^\/]+)\/(.+)/);
		    creds[i]["hook"] = new Discord.WebhookClient(m[1], m[2]);
		    break;
	} 
}

function announcement(text) {
	for (var i=0; i<creds.length; i++) {
		switch(creds[i].service) {
			case "slack":
				creds[i].hook.send({text: text})
				.then(console.log("sent slack "+ text))
			    .catch(console.error);
				break;
			case "rocket":
				rockethook(text,creds[i].url)
				.then(console.log("sent rocket "+ text))
			    .catch(console.error);
				break;
			case "discord":
				creds[i].hook.send(text , {username: creds[i].postID})
				.then(console.log("sent discord "+text))
			    .catch(console.error);
				break;
		} 
	}
}

function getFeed(index) {
	var req = fetch(sources[index].feed);
	req.then(function (res) {
		var feedparser = new FeedParser();
		var fdate = new Date (sources[index].date);
		var latest=fdate;

		feedparser.on('error', function (error) {
		  console.error("FBP" + error);
		  //process.exit();
		});

		feedparser.on('readable', function () {
		  // This is where the action is!
		  var stream = this; // `this` is `feedparser`, which is a stream
		  var meta = this.meta; // **NOTE** the "meta" is always available in the context of the feedparser instance
		  // console.log(sources[index].feed + " posts after "+ sources[index].date)
		  var item;
		  while (item = stream.read()) {
			  //console.log(index, sources[index].date, item.date.toISOString() )
			  if (item.date > fdate) {
				  //console.log(item.title, item.link, item.date);
				  announcement(item.title +" - "+item.link)// +" - "+ item.date.toUTCString())
			  }
			  if (item.date > latest) {
				  latest = item.date;
			  }
			  
		  }
		});

		feedparser.on('close', function () {
		  if (latest>fdate) {
			  sources[index]["date"]=latest.toISOString();
			  fs.writeFile('./sources.json', JSON.stringify(sources), (err) => {
				  if (err) {
					  throw err;
				  }
				  console.log("sources updated for "+index);
			  });
		  }
		});
		feedparser.on('end', function () {
		  if (latest>fdate) {
			  sources[index]["date"]=latest.toISOString();
			  fs.writeFile('./sources.json', JSON.stringify(sources), (err) => {
				  if (err) {
					  throw err;
				  }
				  console.log("sources updated for "+index);
			  });
		  }
		});

			if (res.status !== 200) {
				throw new Error('Bad status code');
			}
			else {
			// The response `body` -- res.body -- is a stream
			res.body.pipe(feedparser);
		  }
		}, function (err) {
		  console.error("REQ" + err);
		  process.exit();
		  // handle any request errors
	});
	
}


var tdate = new Date();
tdate.setTime(tdate.getTime()-86400000);
var stdate=tdate.toISOString();


for (var i=0; i<sources.length; i++) {
	if (sources[i].date == undefined ) {
		sources[i]["date"]=stdate;
	} 
	console.log("Initial get of " + sources[i].feed); 
	getFeed(i);
	setInterval(getFeed, 
				((sources[i].freq == undefined )?900000:(sources[i].freq*60000)),
				1);
}


